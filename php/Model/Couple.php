<?php

namespace App\Buysic\User;

use Illuminate\Database\Eloquent\Model;

class Couple extends Model
{
    
    protected $table = 'user_couples';
    protected $primaryKey = 'user_id';
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'user_id',
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class,
            'user_id',
            'user_id'
        );
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function story()
    {
        return $this->hasOne(
            Story::class,
            'user_id',
            'user_id'
        );
    }
}
