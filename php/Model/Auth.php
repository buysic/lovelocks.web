<?php

namespace App\Buysic\Application\Security;

use App\Buysic\User\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;

class Auth
{

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Generate the JsonWebToken related to the user
     *
     * @param AbstractUser $user
     * @return string
     */
    public function generateJWT($user)
    {
        $data = [
            'userId' => $user->user_id,
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
        ];

        // add the expiration to the JWT
        $data['exp'] = time() + 3600 * config('app.jwtExpireHours');

        $jwt = JWT::encode(
            $data,
            config('app.jwtKey'), config('app.jwtType')
        );

        return $jwt;
    }

    /**
     * Get the JWT contained in the request's header
     *
     * @return array|bool|object|string
     */
    private function getJWT()
    {
        if ($this->request === null) {
            return false;
        }

        $jwt = $this->request->header('Bearer');

        if (empty($jwt)) {
            return false;
        }

        try {
            $jwt = JWT::decode($jwt,
                config('app.jwtKey'), [config('app.jwtType')]);
        } catch(\Exception $e) {
            return false;
        }

        if (empty($jwt->userId) || !is_numeric($jwt->userId)) {
            return false;
        }

        return $jwt;
    }

    /**
     * Check if the JWT contained in the request's header is valid
     *
     * @return bool
     */
    public function checkJWT()
    {
        return $this->getJWT() !== false;
    }

    /**
     * Get an instance of the current logged in user based on the JWT saved in
     * the request's header
     *
     * @return User|false
     */
    public function getLoggedUser()
    {
        $jwt = $this->getJWT();

        if (empty($jwt)) {
            return false;
        }

        return User::where('user_id', $jwt->userId)->first();
    }

}
