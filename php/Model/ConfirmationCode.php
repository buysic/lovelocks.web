<?php

namespace App\Buysic\User;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ConfirmationCode extends Model
{

    protected $table = 'user_confirmation_code';
    protected $primaryKey = 'user_confirmation_code_id';
    
    /**
     *
     * @return bool|mixed
     * @throws \Exception
     */
    public function generate()
    {
        $expire = Carbon::now();
        $expire->add(new \DateInterval('P1D'));

        $code = self::createRandomCode();

        $confirmationCode = self::where('user_id', $this->user_id)->first();
        if (empty($confirmationCode)) {
            $confirmationCode = new self;
        }

        $confirmationCode->user_id = $this->user_id;
        $confirmationCode->code = hash('sha256', $code);
        $confirmationCode->expire_at = $expire;

        // create a new record
        if ($confirmationCode->save()) {
            return $code;
        }

        return false;
    }
    
    /**
     * @param $userId
     * @return bool|mixed
     * @throws \Exception
     */
    public static function generateForUser($userId)
    {
        $confirmationCode = new ConfirmationCode();
        $confirmationCode->user_id = $userId;
        return $confirmationCode->generate();
    }
    
    /**
     * @return int
     * @throws \Exception
     */
    public static function createRandomCode()
    {
        return random_int(10000, 99999);
    }

}
