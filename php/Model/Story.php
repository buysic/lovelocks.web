<?php

namespace App\Buysic\User;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    
    protected $table = 'user_stories';
    protected $primaryKey = 'id';
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class,
            'user_id',
            'user_id'
        );
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function couple()
    {
        return $this->belongsTo(
            Couple::class,
            'user_id',
            'user_id'
        );
    }
}
