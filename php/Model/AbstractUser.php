<?php

namespace App\Buysic\Application\Security;

use App\Buysic\Api\V1\ApiError;
use App\Buysic\User\UserDetails;
use App\Buysic\Utility\Argon\Argon;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractUser extends Model
{
    /**
     * Check if the instance is an admin user
     *
     * @return boolean
     */
    abstract public function isAdmin();

    /**
     * @return string
     */
    public function generateJWT()
    {
        return (new Auth())->generateJWT($this);
    }

    /**
     * @return String
     */
    public function getJwt()
    {
        return $this->generateJWT();
    }
    
    /**
     * Execute the login
     *
     * @param $email
     * @param $password
     * @return $this|array
     * @throws \App\Buysic\Utility\Argon\ArgonException
     */
    public function login($email, $password)
    {
        $user = $this->where([
            'email' => $email,
        ])->first();

        // if user is not found
        if (empty($user)) {
            return [
                'result' => false,
                'errors' => ApiError::ERROR_LOGIN,
            ];
        }
        
        if ($password !== true) {
            // check if the password matches with the saved one
            if (!Argon::passwordVerify($password, $user->password,
                $user->salt)) {
                return [
                    'result' => false,
                    'errors' => ApiError::ERROR_LOGIN,
                ];
            }
        }
    
        // check if the user has been banned
        if ($user->active == 0) {
            return [
                'result' => false,
                'banned' => true,
                'errors' => ApiError::ERROR_LOGIN_BANNED,
            ];
        }
        
        $user->last_login_date = date('Y-m-d H:i:s');
        if (!$user->save()) {
            return [
                'result' => false,
                'errors' => ApiError::ERROR_LOGIN,
            ];
        }
    
        // Check if user details already exist
        $userDetails = UserDetails::where([
            'user_id' => $user->user_id,
        ])->first();
    
        if (empty($userDetails)) {
            $userDetails = new UserDetails();
            $userDetails->user_id = $user->user_id;
            $userDetails->save();
        }
        
        // User successfully logged in, create the JWT
        return [
            'result' => true,
            'user' => $user,
            'jwt' => $user->getJwt(),
        ];
    }

}