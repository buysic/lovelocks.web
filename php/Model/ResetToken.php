<?php

namespace App\Buysic\User;

use App\Buysic\Application\Security\ResetTokenModel;

class ResetToken extends ResetTokenModel
{

    protected $table = 'user_reset_token';
    protected $primaryKey = 'user_reset_token_id';

}
