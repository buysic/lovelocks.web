<?php

namespace App\Buysic\User;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
    protected $table = 'user_orders';
    protected $primaryKey = 'order_id';
    
    protected $fillable = [
        'order_ref', 'user_id', 'customer_id', 'stripe_id', 'card_id', 'card_type',
        'card_country', 'card_last_four', 'amount', 'currency', 'receipt_url',
        'created_at',
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class,
            'user_id',
            'user_id'
        );
    }
}
