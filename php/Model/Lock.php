<?php

namespace App\Buysic\User;

use Illuminate\Database\Eloquent\Model;

class Lock extends Model
{
    const MIN_LOCK_POSITION_ID = 1;
    const MAX_LOCK_POSITION_ID = 10;
    const MIN_FENCE_POSITION_ID = 1;
    const MAX_FENCE_POSITION_ID = 100000;
    
    protected $table = 'user_locks';
    protected $primaryKey = 'lock_id';
    
    protected $fillable = [
        'user_id', 'fence_position_id', 'lock_position_id', 'lock_type_id',
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class,
            'user_id',
            'user_id'
        );
    }
}
