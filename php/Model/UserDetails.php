<?php

namespace App\Buysic\User;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    
    const CONTACT_PREFERENCE_EMAIL_OR_PHONE = 1;
    const CONTACT_PREFERENCE_EMAIL = 2;
    const CONTACT_PREFERENCE_PHONE = 3;

    protected $table = 'user_details';
    protected $primaryKey = 'user_details_id';
    
    protected $fillable = [
        'user_id', 'facebook_url', 'google_url', 'instagram_url', 'twitter_url',
        'contact_preference', 'enable_photo', 'enable_story', 'enable_couple_story',
        'enable_last_name', 'enable_offers', 'enable_facebook_url', 'enable_google_url',
        'enable_instagram_url', 'enable_twitter_url',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_details_id',
        'user_id',
        'created_at',
        'updated_at',
        'contact_preference',
        'date_of_birth',
    ];
    
    /**
     * Get the list of available values for the contactPreference field
     * @return array
     */
    public static function getContactPreferences()
    {
        return [
            self::CONTACT_PREFERENCE_EMAIL_OR_PHONE => "Email or phone call",
            self::CONTACT_PREFERENCE_EMAIL => "Email only",
            self::CONTACT_PREFERENCE_PHONE => "Phone call only",
        ];
    }

}
