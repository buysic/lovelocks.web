<?php

namespace App\Buysic\User;

use App\Buysic\Application\Security\AbstractUser;
use App\Buysic\Utility\Argon\Argon;
use App\Buysic\User\Couple;
use App\Buysic\User\Lock;
use App\Buysic\User\Story;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Snipe\BanBuilder\CensorWords;

class User extends AbstractUser
{

    use SoftDeletes;

    protected $table = 'users';
    protected $primaryKey = 'user_id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'salt',
        'remember_token',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_at',
    ];
    
    /**
     * Get the associated county
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userDetails()
    {
        return $this->hasOne(
            'App\Buysic\User\UserDetails',
            'user_id',
            'user_id'
        );
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function couple()
    {
        return $this->hasOne(
            Couple::class,
            'user_id',
            'user_id'
        );
    }
    
    public function story()
    {
        return $this->hasOne(
            Story::class,
            'user_id',
            'user_id'
        );
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locks()
    {
        return $this->hasMany(
            Lock::class,
            'user_id',
            'user_id'
        );
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(
            Order::class,
            'user_id',
            'user_id'
        );
    }
    
    public static function getRegistrationValidationRules()
    {
        return [
            'email' => 'required|email|max:255',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => self::getPasswordValidationRules(),
            'password_confirmation' => 'required',
        ];
    }
    
    public static function getLoginValidationRules()
    {
        return [
            'email' => 'required',
            'password' => 'required',
        ];
    }
    
    public static function getPasswordValidationRules()
    {
        return 'required|
                min:6|
                regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).+$/|
                confirmed';
    }

    public static function getValidationMessages()
    {
        return [
            'password.regex' =>
                'The password must contain at least an uppercase, '
                . 'a lowercase character and at least a number',
        ];
    }

    public static function getCaptchaValidation($captchaResponse)
    {
        $recaptchaUrl = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptchaSecret = config('app.recaptcha.secret_key');
        
        if (!empty($captchaResponse)) {
            $verifyResponse = file_get_contents($recaptchaUrl . '?secret='
                . $recaptchaSecret . '&response=' . $captchaResponse
            );
            
            $recaptcha = json_decode($verifyResponse);
            if ($recaptcha->success && $recaptcha->score >= 0.5) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Scope a query to only include active records
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * Check if the instance is an admin user
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return false;
    }
    
    /**
     * Verify the passed password.
     *
     * If the user doesn't already have a salted password, change it.
     *
     * @param $password
     * @param bool $overrideIfNecessary
     * @return bool
     * @throws \App\Buysic\Utility\Argon\ArgonException
     */
    public function verifyPassword($password, $overrideIfNecessary = true)
    {
        // check if this user has already the new password or it has only the
        // phase one md5 password
        if (empty($this->salt)) {
            // need the new password encryption. Before check if the old pwd is correct
            if ($this->password !== md5($password)) {
                return false;
            }

            if ($overrideIfNecessary) {
                // the password is correct, save it in the new format (Argon)
                $this->setPassword($password);
                return $this->save();
            }

        } elseif (!Argon::passwordVerify($password, $this->password, $this->salt)) {
            return false;
        }

        return true;
    }
    
    /**
     * Set the password after encrypting it with Argon
     *
     * @param $password
     * @return void
     * @throws \App\Buysic\Utility\Argon\ArgonException
     */
    public function setPassword($password)
    {
        $argonHash = Argon::passwordHash($password);
        $this->password = $argonHash[Argon::RESULT_KEY_HASH];
        $this->salt = $argonHash[Argon::RESULT_KEY_SALT];
    }
    
    /**
     * Get social url based on parsed social network parameter
     *
     * @param $url
     * @param $social
     * @return string
     */
    public function getSocialUrl($url, $social)
    {
        // Strip off the url, get the last part and treat it as a social id
        $url = trim($url,'/');
        $url = preg_replace('/\?.*/', '', $url);
        $url = explode('/', $url);
        $url = end($url);
    
        $socialUrl = null;
        if (!empty($social)) {
            if ($social === 'facebook') {
                $socialUrl = $url ? ('https://facebook.com/' . $url) : null;
            } elseif ($social === 'google') {
                $socialUrl = $url ? ('https://plus.google.com/' . $url) : null;
            } elseif ($social === 'instagram') {
                $socialUrl = $url ? ('https://instagram.com/' . $url) : null;
            } elseif ($social === 'twitter') {
                $socialUrl = $url ? ('https://twitter.com/' . $url) : null;
            }
        }
        
        return $socialUrl;
    }
    
    /**
     * Update the user and user details table with the data passed from the a
     * post request.
     *
     * @param $params
     * @param bool $overrideNulls
     * @return bool
     */
    public function updateFromPost($params, $overrideNulls = false)
    {
        $censor = new CensorWords;
        $censor->setReplaceChar("*");
        
        // Get also the user details object if exists, or create a new one
        $userDetails = $this->userDetails;
        if (empty($userDetails)) {
            $userDetails = new UserDetails;
            $userDetails->user_id = $this->user_id;
        }
    
        // Get also the user stories object if exists, or create a new one
        $userStory = $this->story;
        if (empty($userStory)) {
            $userStory = new Story;
            $userStory->user_id = $this->user_id;
        }
        
        if (isset($params['email'])) {
            $this->email = $params['email'];
        }
        
        if (isset($params['first_name'])) {
            $this->first_name = $censor->censorString($params['first_name'])['clean'];
        }
        
        if (isset($params['last_name'])) {
            $this->last_name = $censor->censorString($params['last_name'])['clean'];
        }
        
        if (isset($params['get_news'])) {
            $this->newsletter = $params['get_news'];
        } elseif($overrideNulls) {
            $this->newsletter = 0;
        }
    
        // Social profile urls
        
        if (isset($params['facebook_url'])) {
            $userDetails->facebook_url = $this->getSocialUrl(
                $params['facebook_url'],
                'facebook'
            );
        } elseif($overrideNulls) {
            $userDetails->facebook_url = null;
        }
    
        if (isset($params['google_url'])) {
            $userDetails->google_url = $this->getSocialUrl(
                $params['google_url'],
                'google'
            );
        } elseif($overrideNulls) {
            $userDetails->google_url = null;
        }
    
        if (isset($params['instagram_url'])) {
            $userDetails->instagram_url = $this->getSocialUrl(
                $params['instagram_url'],
                'instagram'
            );
        } elseif($overrideNulls) {
            $userDetails->instagram_url = null;
        }
    
        if (isset($params['twitter_url'])) {
            $userDetails->twitter_url = $this->getSocialUrl(
                $params['twitter_url'],
                'twitter'
            );
        } elseif($overrideNulls) {
            $userDetails->twitter_url = null;
        }
        
        // User preferences
        
        if (isset($params['contact_preference_id'])) {
            $userDetails->contact_preference = $params['contact_preference_id'];
        } elseif($overrideNulls) {
            $userDetails->contact_preference = null;
        }
        
        if (isset($params['enable_photo'])) {
            $userDetails->enable_photo = $params['enable_photo'];
        } elseif($overrideNulls) {
            $userDetails->enable_photo = null;
        }
    
        if (isset($params['enable_story'])) {
            $userDetails->enable_story = $params['enable_story'];
        } elseif($overrideNulls) {
            $userDetails->enable_story = null;
        }
    
        if (isset($params['enable_last_name'])) {
            $userDetails->enable_last_name = $params['enable_last_name'];
        } elseif($overrideNulls) {
            $userDetails->enable_last_name = null;
        }
    
        if (isset($params['enable_couple_story'])) {
            $userDetails->enable_couple_story = $params['enable_couple_story'];
        } elseif($overrideNulls) {
            $userDetails->enable_couple_story = null;
        }
    
        if (isset($params['enable_facebook_url'])) {
            $userDetails->enable_facebook_url = $params['enable_facebook_url'];
        } elseif($overrideNulls) {
            $userDetails->enable_facebook_url = null;
        }
    
        if (isset($params['enable_google_url'])) {
            $userDetails->enable_google_url = $params['enable_google_url'];
        } elseif($overrideNulls) {
            $userDetails->enable_google_url = null;
        }
    
        if (isset($params['enable_instagram_url'])) {
            $userDetails->enable_instagram_url = $params['enable_instagram_url'];
        } elseif($overrideNulls) {
            $userDetails->enable_instagram_url = null;
        }
    
        if (isset($params['enable_twitter_url'])) {
            $userDetails->enable_twitter_url = $params['enable_twitter_url'];
        } elseif($overrideNulls) {
            $userDetails->enable_twitter_url = null;
        }
        
        if (isset($params['enable_offers'])) {
            $userDetails->enable_offers = $params['enable_offers'];
        } elseif($overrideNulls) {
            $userDetails->enable_offers = null;
        }
        
        if (isset($params['dob'])) {
            $userDetails->date_of_birth = $params['dob'];
        } elseif($overrideNulls) {
            $userDetails->date_of_birth = null;
        }
    
        if (isset($params['story'])) {
            $userStory->story = $censor->censorString($params['story'])['clean'];
        } elseif($overrideNulls) {
            $userStory->story = null;
        }
        
        if (!$this->save()
            || !$userDetails->save()
            || !$userStory->save())
        {
            return false;
        }

        return true;
    }
}
