<?php

namespace App\Http\Controllers;

use App\Buysic\Api\V1\ApiError;
use App\Buysic\Application\Security\Auth;
use App\Buysic\Mail\PaymentConfirmation;
use App\Buysic\User\Lock;
use App\Buysic\User\Order;
use App\Buysic\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Stripe\Customer;
use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Token;

class PaymentController extends BaseApiController
{
    
    /**
     * Stripe payment
     * We receive data from customer and create a new transaction here
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *        |\Illuminate\Http\Response
     * @throws \Exception
     */
    public function stripePayment(Request $request)
    {
        $params = $request->all();
    
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'token' => 'required',
                'full_name' => 'required',
                'cvc' => 'required|min:3|max:4',
                'lock_type_id' => 'required|numeric',
                'lock_position_id' => 'required|numeric',
                'fence_position_id' => 'required|numeric',
            ]
        );
    
        if ($validator->fails()) {
            return response([
                'result' => false,
                'errors' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // Get logged in user
        $user = (new Auth($request))->getLoggedUser();
        if (empty($user)) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_NOT_SIGNED_IN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // Set up Stripe instance
        Stripe::setApiKey(config('app.stripeApi.secret_key'));
        
        $token = $params['token'] ?? null;
        $userId = $user->user_id ?? null;
        $email = $user->email ?? null;
        $fencePositionId = $params['fence_position_id'] ?? null;
        $lockPositionId = $params['lock_position_id'] ?? null;
        $lockTypeId = $params['lock_type_id'] ?? null;

        $orderRef = 'LVS-'. substr(date('y'), -2);
        if (!empty($fencePositionId) && !empty($lockPositionId)) {
            $orderRef .= '/'
                . $fencePositionId
                . '-'
                . $lockPositionId
                . '-'
                . $lockTypeId;
        }
    
        // Check if user lock exists
        $lock = Lock::where([
            'lock_position_id' => $lockPositionId,
            'fence_position_id' => $fencePositionId,
        ])->whereNotNull('user_id')->first();
    
        if (!empty($lock)) {
            return response([
                'error' => ApiError::ERROR_USER_LOCK_ALREADY_PURCHASED,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
    
        // Check if lock position is correct
        if ($lockPositionId < Lock::MIN_LOCK_POSITION_ID
            || $lockPositionId > Lock::MAX_LOCK_POSITION_ID
        ) {
            return response([
                'error' => ApiError::ERROR_USER_LOCK_CANNOT_PURCHASE,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
    
        // Check if fence position is correct
        if ($fencePositionId < Lock::MIN_FENCE_POSITION_ID
            || $fencePositionId > Lock::MAX_FENCE_POSITION_ID
        ) {
            return response([
                'error' => ApiError::ERROR_USER_LOCK_CANNOT_PURCHASE,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        try {
            $customerId = null;
            $card = null;
            $orders = $user->orders;

            // Check if customer already exists
            if ($orders->isEmpty()) {
                $customer = Customer::create(array(
                    'email' => $email,
                    'description' => 'Customer for ' . $email,
                    'source' => $token,
                ));
                $customerId = $customer->id;
            } else {
                $orders = json_decode(json_encode($orders[0]));
                $customerId = $orders->customer_id;
            }
    
            // Retrieve customer data
            $customer = Customer::retrieve($customerId);
            
            // Retrieve token data
            $token = Token::retrieve($token);
            
            // Add the card, but only if it does not exist
            foreach ($customer->sources['data'] as $source) {
                if ($source['fingerprint'] === $token['card']->fingerprint) {
                    $card = $source;
                }
            }

            // If no card found, add a new card to the customer
            if ($card === null) {
                $card = $customer->sources->create([
                    'source' => $token,
                ]);
            }
    
            // Set the available card as default
            $customer->default_source = $card->id;
            $customer->save();
            
            // Charge a customer
            $charge = Charge::create([
                'amount' => config('app.lock_price') * 100,
                'currency' => 'usd',
                'customer' => $customerId,
                'metadata' => [
                    'order_id' => $orderRef,
                    'email' => $email,
                    'fence_position_id' => $fencePositionId,
                    'lock_position_id' => $lockPositionId,
                    'lock_type_id' => $lockTypeId,
                    'description' => 'Virtual lock has been purchased',
                ],
            ]);
        } catch(\Stripe\Error\Card $e) {
            return response([
                'result' => false,
                'error' => 'Your card was declined. Please try again.',
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        } catch (\Stripe\Error\ApiConnection $e) {
            return response([
                'result' => false,
                'error' => 'There is an internet connection problem, please '
                . 'try again later.',
            ], ApiError::CODE_PARAM_ERROR);
        } catch (\Stripe\Error\Base $e) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_GENERIC_FAIL,
            ], ApiError::CODE_PARAM_ERROR);
        } catch (\Exception $e) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_GENERIC_FAIL,
            ], ApiError::CODE_PARAM_ERROR);
        }
        
        // Retrieve data from charge
        $paymentDetails = Charge::retrieve($charge->id);
        $cardDetails = $charge->source ?? null;
        
        $stripeId = $paymentDetails ? $paymentDetails->id : null;
        $amount = $paymentDetails ? $paymentDetails->amount / 100 : null;
        $currency = $paymentDetails ? $paymentDetails->currency : null;
        $receiptUrl = $paymentDetails ? $paymentDetails->receipt_url : null;
        $createdAt = $paymentDetails ? $paymentDetails->created : null;
    
        $cardId = $cardDetails ? $cardDetails->id : null;
        $cardType = $cardDetails ? $cardDetails->brand : null;
        $cardCountry = $cardDetails ? $cardDetails->country : null;
        $cardLast4 = $cardDetails ? $cardDetails->last4 : null;
        $cardCvcCheck = $cardDetails ? $cardDetails->cvc_check : null;
        
        $orderDetails = [
            'order_ref' => $orderRef,
            'user_id' => $userId,
            'customer_id' => $customerId,
            'stripe_id' => $stripeId,
            'card_id' => $cardId,
            'card_type' => $cardType,
            'card_country' => $cardCountry,
            'card_last4' => $cardLast4,
            'cvc_check' => $cardCvcCheck,
            'amount' => $amount,
            'currency' => $currency,
            'receipt_url' => $receiptUrl,
            'created_at' => $createdAt,
        ];
        
        $userLockDetails = [
            'user_id' => $userId,
            'fence_position_id' => $fencePositionId,
            'lock_position_id' => $lockPositionId,
            'lock_type_id' => $lockTypeId,
        ];
        
        // Save order details
        $this->storeUserOrder($orderDetails);
    
        // Save user lock details
        $this->storeUserLock($userLockDetails);
        
        // Send a confirmation email to user
        $this->sendPaymentConfirmation($user);
        
        return response([
            'result' => true,
        ]);
    }
    
    /**
     * Create a new order
     *
     * @param array $orderDetails
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *      |\Illuminate\Http\Response
     */
    private function storeUserOrder($orderDetails = [])
    {
        DB::beginTransaction();
        try {
            $order = new Order;
            $order->fill($orderDetails);
            
            if (!$order->save()) {
                DB::rollBack();
                return response([
                    'result' => false,
                    'error' => ApiError::ERROR_PAYMENT_ORDER_FAIL,
                ])->setStatusCode(ApiError::CODE_ERROR);
            }
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response([
                'result' => false,
                'error' => ApiError::ERROR_PAYMENT_ORDER_FAIL,
            ])->setStatusCode(ApiError::CODE_ERROR);
        }
    }
    
    /**
     * Update purchased lock for user
     *
     * @param array $userLockDetails
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *      |\Illuminate\Http\Response
     */
    private function storeUserLock($userLockDetails = [])
    {
        try {
            // Check if user lock exists
            $lock = Lock::where([
                'lock_position_id' => $userLockDetails['lock_position_id'],
                'fence_position_id' => $userLockDetails['fence_position_id'],
            ])->whereNull('user_id')->first();
            
            if (empty($lock)) {
                return response([
                    'error' => ApiError::ERROR_USER_LOCK_NOT_EXISTS,
                ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
            }
    
            // Fill the details and assign purchased lock to user
            $lock->fill($userLockDetails);
            
            if (!$lock->save()) {
                return response([
                    'result' => false,
                    'error' => ApiError::ERROR_CREATE_USER_LOCK_FAIL,
                ])->setStatusCode(ApiError::CODE_ERROR);
            }
        } catch (\Exception $e) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_CREATE_USER_LOCK_FAIL,
            ])->setStatusCode(ApiError::CODE_ERROR);
        }
    }
    
    /**
     * Send a confirmation email regarding the successful payment
     *
     * @param $user
     * @throws \Exception
     */
    private function sendPaymentConfirmation($user)
    {
        $emailUserTemplate = new PaymentConfirmation($user);
        
        // send the email
        Mail::to($user->email)->send($emailUserTemplate);
    }
}
