<?php

namespace App\Http\Controllers;

use App\Buysic\Api\V1\ApiError;
use App\Buysic\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends BaseApiController
{
    
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)
            ->stateless()
            ->redirect();
    }
    
    /**
     * @param Request $request
     * @return User|array
     * @throws \App\Buysic\Utility\Argon\ArgonException
     */
    public function handleProviderCallback(Request $request)
    {
        $provider = $request->provider;

        try {
            $data = Socialite::with($provider)
                ->stateless()
                ->userFromToken($request->access_token);
        } catch (\Exception $e) {
            return response([
                'result' => false,
                'error' => 'Something went wrong with a social authentication',
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }

        $authUser = $this->findOrCreateUser($data, $provider);
        
        $token = null;
        if (!empty($authUser)) {
            $token = (new User)->login($authUser->email, true);
        }
    
        return response([
            'result' => true,
            'user' => $authUser,
            'jwt' => $token['jwt'],
        ]);
    }
    
    public function findOrCreateUser($data, $provider)
    {
        $params = $data->user;

        if (!$this->isAllowedProvider($provider)) {
            return null;
        }
        
        // Array of received data
        $providerUser = User::where($provider . '_id', $params['id'])->first();
        if ($providerUser) {
            return $providerUser;
        }
    
        $user = User::where('email', $params['email'])->first();
        
        if ($user) {
            if ($provider === 'facebook') {
                $user->facebook_id = $params['id'];
                $user->facebook_image = $data->avatar_original ?? $data->getAvatar();
            } else if ($provider === 'google') {
                $user->google_id = $params['id'];
                $user->google_image = $data->getAvatar();
            }
            
            if (!$user->save()) {
                return response([
                    'result' => false,
                    'errors' => ApiError::ERROR_REGISTRATION_FAIL,
                ]);
            }
            
            return $user;
        }
        
        DB::beginTransaction();
        try {
            // Create a new user
            $user = new User;
            
            if ($provider === 'facebook') {
                $user->facebook_id = $params['id'];
                $user->facebook_image = $data->avatar_original ?? $data->getAvatar();
                $user->first_name = $params['first_name'] ?? null;
                $user->last_name = $params['last_name'] ?? null;
            } else if ($provider === 'google') {
                $user->google_id = $params['id'];
                $user->google_image = $data->getAvatar();
                $user->first_name = $params['given_name'] ?? null;
                $user->last_name = $params['family_name'] ?? null;
            }
            
            $user->email = $params['email'] ?? null;
            $user->newsletter = 1;
            $user->active = 1;
    
            if (!$user->save()) {
                DB::rollBack();
                return response([
                    'result' => false,
                    'errors' => ApiError::ERROR_REGISTRATION_FAIL,
                ])->setStatusCode(ApiError::CODE_ERROR);
            }
            
            DB::commit();
        } catch(\Exception $e) {
            DB::rollBack();
            return response([
                'result' => false,
                'errors' => ApiError::ERROR_REGISTRATION_FAIL,
            ])->setStatusCode(ApiError::CODE_ERROR);
        }
        
        return $user;
    }
    
    /** Get all social providers
     *
     * @return array
     */
    protected function socialProviders()
    {
        return [
            'facebook',
            'google',
        ];
    }
    
    /**
     * Check if provider is allowed to be used
     *
     * @param $provider
     * @return bool
     */
    protected function isAllowedProvider($provider)
    {
        return in_array($provider, $this->socialProviders());
    }
    
}
