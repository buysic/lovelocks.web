<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Response;

use App\Buysic\Api\V1\ApiError;

class BaseApiController extends Controller
{

    /**
     * Return an "element not found" error response
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *        |\Symfony\Component\HttpFoundation\Response
     */
    protected function notFound()
    {
        return response([
            'result' => false,
            'errors' => ApiError::ERROR_ELEMENT_NOT_FOUND,
        ])->setStatusCode(ApiError::CODE_NOT_FOUND);
    }

    /**
     * Return an "not allowed" error response
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *        |\Symfony\Component\HttpFoundation\Response
     */
    protected function notAllowed()
    {
        return response([
            'result' => false,
            'errors' => ApiError::ERROR_JWT_REQUIRED,
        ])->setStatusCode(ApiError::CODE_AUTH_ERROR);
    }

    /**
     * Return a generic error response
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *        |\Symfony\Component\HttpFoundation\Response
     */
    protected function fail()
    {
        return response([
            'result' => false,
            'errors' => ApiError::ERROR_GENERIC_FAIL,
        ])->setStatusCode(ApiError::CODE_ERROR);
    }

    /**
     * Return a validation error response
     * @param $validator
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *          |\Symfony\Component\HttpFoundation\Response
     */
    protected function validationError($validator)
    {
        return $this->errors($validator->errors());
    }

    /**
     * @param array|string $errors
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *          |\Symfony\Component\HttpFoundation\Response
     */
    protected function errors($errors)
    {
        return response([
            'result' => false,
            'errors' => $errors,
        ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *          |\Symfony\Component\HttpFoundation\Response
     */
    protected function redirectToId($id)
    {
        return response([
            'result' => false,
            'redirectToId' => $id,
        ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     *          |\Symfony\Component\HttpFoundation\Response
     */
    protected function success()
    {
        return response([
            'result' => true,
        ]);
    }
}
