<?php

namespace App\Http\Controllers;

use App\Buysic\Api\V1\ApiError;
use App\Buysic\Application\Security\Auth;
use App\Buysic\Mail\ResetPasswordEmail;
use App\Buysic\Mail\ReviewAcceptedConfirmation;
use App\Buysic\Mail\ReviewDeclinedConfirmation;
use App\Buysic\Mail\UserAlreadyExistsEmail;
use App\Buysic\Mail\UserInviteRequestEmail;
use App\Buysic\Mail\WelcomeEmail;
use App\Buysic\User\ConfirmationCode;
use App\Buysic\User\Couple;
use App\Buysic\User\Lock;
use App\Buysic\User\ResetToken;
use App\Buysic\User\User;
use App\Buysic\User\UserDetails;
use App\Buysic\Utility\Argon\Argon;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class UserController extends BaseApiController
{
    
    /**
     * Register a new user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     * @throws \Exception
     */
    public function register(Request $request)
    {
        $params = $request->all();
        
        // validate fields
        $validator = Validator::make(
            $params,
            User::getRegistrationValidationRules(),
            User::getValidationMessages()
        );
        
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // check if the email is already subscribed
        $checkEmail = User::where('email', $params['email'])->first();
        
        if (!empty($checkEmail)) {
            // check if this user is not active
            if ($checkEmail->active !== 1) {
                return $this->errors(new MessageBag([
                    'error' => ApiError::ERROR_REGISTRATION_NOT_ACTIVE
                ]));
            }
            
            // try to login the user
            $user = User::active()->where([
                'email' => $params['email'],
            ])->first();
            
            if ($user->verifyPassword($params['password'])) {
                
                if ($user->confirmed_email !== 1) {
                    // the user already exists but he didn't confirmed his email
                    $this->sendConfirmationCode($user);
                    
                    // create the jwt for the user
                    return response([
                        'result' => true
                    ]);
                }
                
                // log the executed login
                $user->last_login_date = Carbon::now(new \DateTimeZone('Europe/London'));
                $user->save();
                
                // create the jwt for the user
                return response([
                    'result' => true,
                    'jwt' => $user->getJwt()
                ]);
            }
            
            // send an email that send the user to the reset password
            $this->sendAlreadyExistingEmail($checkEmail);
            
            // create the jwt for the user
            return response([
                'result' => true
            ]);
        }
        
        // Validate reCaptcha response
        $captcha = $params['captcha-response'] ?? null;
        if (!User::getCaptchaValidation($captcha)) {
            return $this->errors(new MessageBag([
                'recaptcha' => ApiError::ERROR_RECAPTCHA_CONFIRMATION,
            ]));
        }
        
        DB::beginTransaction();
        try {
            // create a new user
            $user = new User();
            $user->email = $params['email'];
            $user->first_name = $params['first_name'];
            $user->last_name = $params['last_name'];
            $user->newsletter = 1;
            $user->active = 1;
            
            $argonHash = Argon::passwordHash($params['password']);
            $user->password = $argonHash[Argon::RESULT_KEY_HASH];
            $user->salt = $argonHash[Argon::RESULT_KEY_SALT];
            
            if (!$user->save()) {
                DB::rollBack();
                return response([
                    'result' => false,
                    'error' => ApiError::ERROR_REGISTRATION_FAIL,
                ])->setStatusCode(ApiError::CODE_ERROR);
            }
            
            $userDetails = new UserDetails();
            $userDetails->user_id = $user->user_id;
            $userDetails->save();
            
            $this->sendConfirmationCode($user);
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response([
                'result' => false,
                'error' => ApiError::ERROR_REGISTRATION_FAIL,
            ])->setStatusCode(ApiError::CODE_ERROR);
        }
        
        // create the jwt for the user
        return response([
            'result' => true
        ]);
    }
    
    /**
     * Login an existing user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function login(Request $request)
    {
        $params = $request->all();
        
        // check data in the db and create the jwt if the user has been found
        $validator = Validator::make(
            $params,
            User::getLoginValidationRules()
        );
        
        if ($validator->fails()) {
            return response([
                'result' => false,
                'errors' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        $user = User::where([
            'email' => $params['email'],
        ])->first();
        
        if (empty($user)) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_LOGIN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // verify the password
        if (!$user->verifyPassword($params['password'])) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_LOGIN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // check if the user has been banned
        if ($user->active === 0) {
            return response([
                'result' => false,
                'banned' => true,
                'error' => ApiError::ERROR_LOGIN_NOT_ACTIVE,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // check if the user confirmed his email
        if ($user->confirmed_email === 0) {
            return response([
                'result' => false,
                'needToConfirmEmail' => true,
                'error' => ApiError::ERROR_EMAIL_NOT_VALIDATED,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // Validate reCaptcha response
        $captcha = $params['captcha-response'] ?? null;
        if (!User::getCaptchaValidation($captcha)) {
            return $this->errors(new MessageBag([
                'recaptcha' => ApiError::ERROR_RECAPTCHA_CONFIRMATION,
            ]));
        }

        // Check if user details already exist
        $userDetails = UserDetails::where([
            'user_id' => $user->user_id,
        ])->first();
        
        if (empty($userDetails)) {
            $userDetails = new UserDetails();
            $userDetails->user_id = $user->user_id;
            $userDetails->save();
        }
        
        // log the executed login
        $user->last_login_date = Carbon::now(new \DateTimeZone('Europe/London'));
        $user->save();
        
        // create the jwt for the user
        return response([
            'result' => true,
            'user' => $user,
            'jwt' => $user->getJwt()
        ]);
    }
    
    /**
     * Confirm the user email
     *
     * @param Request $request
     * @return $this
     */
    public function confirmEmail(Request $request)
    {
        $params = $request->all();
    
        try {
            $params['email'] = Crypt::decryptString($params['email']);
        } catch (\Exception $e) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_RESET_PASSWORD_TOKEN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'email' => 'required|email',
                'code' => 'required|min:64',
            ]
        );
        
        if ($validator->fails()) {
            return response([
                'result' => false,
                'errors' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        $user = User::where([
            'email' => $params['email'],
        ])->first();
        
        if (empty($user)) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_CONFIRMATION_CODE,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        $confirmationCode = ConfirmationCode::where([
            'user_id' => $user->user_id,
            'code' => $params['code'],
        ])->where(
            'expire_at', '>', Carbon::now()
        )->first();
        
        if (empty($confirmationCode)) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_CONFIRMATION_CODE,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // validate the email for this user
        $user->confirmed_email = 1;
        // log the executed login
        $user->last_login_date = Carbon::now(new \DateTimeZone('Europe/London'));
        
        if (!$user->save() || !$confirmationCode->delete()) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_CONFIRMATION_FAIL,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // create the jwt for the user
        return response([
            'result' => true,
            'jwt' => $user->getJwt()
        ]);
    }
    
    /**
     * Reset password for existing user that has a reset token
     *
     * @param Request $request
     * @return UserController|\Illuminate\Contracts\Routing\ResponseFactory
     * @throws \App\Buysic\Utility\Argon\ArgonException
     */
    public function resetPassword(Request $request)
    {
        $params = $request->all();
        
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'email' => 'required',
                'password' => User::getPasswordValidationRules(),
                'password_confirmation' => 'required',
                'token' => 'required',
            ]
        );
        
        if ($validator->fails()) {
            return response([
                'result' => false,
                'errors' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
    
        try {
            $email = Crypt::decryptString($params['email']);
        } catch (\Exception $e) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_RESET_PASSWORD_TOKEN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        $user = User::where('email', $email)->first();
 
        if (empty($user)) {
            return response([
                'error' => ApiError::ERROR_RESET_PASSWORD_TOKEN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        $resetToken = ResetToken::where([
            'user_id' => $user->user_id,
            'token' => hash('sha256', $params['token']),
        ])->where(
            'expire_at', '>=', Carbon::now()
        )->first();
        
        if (empty($resetToken)) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_RESET_PASSWORD_TOKEN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // Validate reCaptcha response
        $captcha = $params['captcha-response'] ?? null;
        if (!User::getCaptchaValidation($captcha)) {
            return $this->errors(new MessageBag([
                'recaptcha' => ApiError::ERROR_RECAPTCHA_CONFIRMATION,
            ]));
        }
        
        // save the new password
        $argonHash = Argon::passwordHash($params['password']);
        $user->password = $argonHash[Argon::RESULT_KEY_HASH];
        $user->salt = $argonHash[Argon::RESULT_KEY_SALT];
        
        if (!$user->save() || !$resetToken->delete()) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_RESET_PASSWORD_FAIL,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // login the user
        return response([
            'result' => true
        ]);
    }
    
    /**
     * Resend confirmation email code
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|null
     *      |\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function resendCode(Request $request)
    {
        $params = $request->all();
        
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'email' => 'required',
            ]
        );
        
        if ($validator->fails()) {
            return response([
                'result' => false,
                'error' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // check if the email is already subscribed
        $user = User::where('email', $params['email'])->first();
        
        if (empty($user)) {
            return response([
                'result' => true
            ]);
        }
        
        // Validate reCaptcha response
        $captcha = $params['captcha-response'] ?? null;
        if (!User::getCaptchaValidation($captcha)) {
            return $this->errors(new MessageBag([
                'recaptcha' => ApiError::ERROR_RECAPTCHA_CONFIRMATION,
            ]));
        }
        
        $this->sendConfirmationCode($user);
        
        // create the jwt for the user
        return response([
            'result' => true
        ]);
    }
    
    /**
     * Resend reset password token
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function forgot(Request $request)
    {
        $params = $request->all();
        
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'email' => 'required',
            ]
        );
        
        if ($validator->fails()) {
            return response([
                'result' => false,
                'errors' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // Validate reCaptcha response
        $captcha = $params['captcha-response'] ?? null;
        if (!User::getCaptchaValidation($captcha)) {
            return $this->errors(new MessageBag([
                'recaptcha' => ApiError::ERROR_RECAPTCHA_CONFIRMATION,
            ]));
        }
        
        // check if the email is already subscribed
        $user = User::active()->where('email', $params['email'])->first();
        
        if (empty($user)) {
            // TODO: we could send an email to this address saying that there is
            // no user subscribed with this email
            
            // don't do anything, respond with success
            return response([
                'result' => true
            ]);
        }
        
        $this->sendResetPasswordEmail($user);
        
        // create the jwt for the user
        return response([
            'result' => true
        ]);
    }
    
    /**
     * Get user's profile
     *
     * @param Request $request
     * @return $this
     *      |\Illuminate\Contracts\Routing\ResponseFactory
     *      |\Symfony\Component\HttpFoundation\Response
     */
    public function profile(Request $request)
    {
        // get the logged user
        $user = app()->make('\App\Buysic\User\User');
        $user = User::with(['userDetails', 'story', 'locks'])
            ->where('user_id', $user->user_id)
            ->get()
            ->first();
        
        if (!$user) {
            return response([
                'result' => false
            ])->setStatusCode(ApiError::CODE_AUTH_ERROR);
        }
        
        $couple = Couple::with(['user'])
            ->where('couple_id', $user->user_id)
            ->get()
            ->first();
    
        if (!empty($user->couple)) {
            $couple = User::find($user->couple->couple_id);
            $couple->story;
        }
        
        return response([
            'result' => true,
            'user' => $user,
            'couple' => $couple,
        ]);
    }
    
    /**
     * Get user inviter's profile
     *
     * @param Request $request
     * @return $this
     *      |\Illuminate\Contracts\Routing\ResponseFactory
     *      |\Symfony\Component\HttpFoundation\Response
     */
    public function inviterProfile(Request $request)
    {
        $params = $request->all();
        
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'reviewToken' => 'required',
            ]
        );
        
        if ($validator->fails()) {
            return response([
                'result' => false,
                'error' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        $userInviter = User::with(['userDetails', 'couple', 'story'])
            ->where([
                'affiliate_token' => $params['reviewToken'],
            ])->first();
    
 
        if (empty($userInviter)) {
            return response([
                'result' => false,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        return response([
            'result' => true,
            'user' => $userInviter,
        ]);
    }
    
    /**
     * Update user's profile
     *
     * @param Request $request
     * @return UserController|bool|\Illuminate\Contracts\Routing\ResponseFactory
     */
    public function updateProfile(Request $request)
    {
        $params = $request->all();
        
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'email|max:255',
                'story' => 'nullable|min:100|max:500',
                'facebook_url' => 'nullable|max:255',
                'google_url' => 'nullable|max:255',
                'instagram_url' => 'nullable|max:255',
                'twitter_url' => 'nullable|max:255',
                'contact_preference_id' => 'nullable|numeric',
                'dob' => 'nullable|date',
            ],
            User::getValidationMessages()
        );
        
        if ($validator->fails()) {
            return $this->validationError($validator);
        }
    
        // Validate reCaptcha response
        $captcha = $params['captcha-response'] ?? null;
        if (!User::getCaptchaValidation($captcha)) {
            return $this->errors(new MessageBag([
                'recaptcha' => ApiError::ERROR_RECAPTCHA_CONFIRMATION,
            ]));
        }
        
        // get the logged user
        /* @var $user \App\Buysic\User\User */
        $user = app()->make('\App\Buysic\User\User');
        
        // check if there is a param that says to not override the null values
        $overrideNulls = $request->post('overrideNulls', true);
        
        if (!$user->updateFromPost($params, $overrideNulls)) {
            return $this->fail();
        }
        
        return response([
            'result' => true
        ]);
    }
    
    /**
     * Reset password for existing user that has a reset token
     *
     * @param Request $request
     * @return UserController|\Illuminate\Contracts\Routing\ResponseFactory
     * @throws \App\Buysic\Utility\Argon\ArgonException
     */
    public function changePassword(Request $request)
    {
        $params = $request->all();
        
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'old_password' => 'required',
                'password' => User::getPasswordValidationRules(),
                'password_confirmation' => 'required',
            ]
        );
        
        if ($validator->fails()) {
            return response([
                'result' => false,
                'errors' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // get the logged user
        /* @var $user \App\Buysic\User\User */
        $user = app()->make('\App\Buysic\User\User');
        if (!$user) {
            return response([
                'result' => false
            ])->setStatusCode(ApiError::CODE_AUTH_ERROR);
        }
        
        // verify the old password without saving the new if correct
        if (!$user->verifyPassword($params['old_password'], false)) {
            return $this->errors(new MessageBag([
                'error' => 'Your old password is wrong'
            ]));
        }
        
        // save the new password
        $user->setPassword($params['password']);
        if (!$user->save()) {
            return $this->errors(new MessageBag([
                'error' => ApiError::ERROR_RESET_PASSWORD_FAIL,
            ]));
        }
        
        // login the user
        return response([
            'result' => true
        ]);
    }
    
    /**
     * Delete the user account
     *
     * @param Request $request
     * @return UserController|\Illuminate\Contracts\Routing\ResponseFactory
     */
    public function deleteAccount(Request $request)
    {
        // get the logged user
        /* @var $user \App\Buysic\User\User */
        $user = app()->make('\App\Buysic\User\User');
        
        // set the active to 2 to be able to get recognise a user that delete his
        // account by his self
        $user->active = 2;
        
        DB::beginTransaction();
        
        try {
            if (!$user->save()) {
                DB::rollBack();
                return $this->fail();
            }
            
            if (!$user->delete()) {
                DB::rollBack();
                return $this->fail();
            }
            
            DB::commit();
            
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->fail();
        }
        
        // login the user
        return response([
            'result' => true
        ]);
    }
    
    /**
     * Send request to user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|null|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function sendRequest(Request $request)
    {
        $params = $request->all();
        
        // if inviter is signed in
        $userInviter = (new Auth($request))->getLoggedUser();
        if (empty($userInviter)) {
            return response([
                'error' => ApiError::ERROR_NOT_SIGNED_IN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        // if inviter did not confirmed his email
        if ($userInviter->confirmed_email === 0) {
            return response([
                'error' => ApiError::ERROR_EMAIL_NOT_VALIDATED,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        // if inviter is not active
        if ($userInviter->active !== 1) {
            return response([
                'error' => ApiError::ERROR_LOGIN_NOT_ACTIVE,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // if inviter already sent a request to someone else
        if ($userInviter->affiliate_token) {
            return response([
                'error' => ApiError::ERROR_REVIEW_REQUEST_ALREADY_SENT,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'lock_id' => 'required|numeric',
                'lock_position_id' => 'required|numeric',
                'fence_position_id' => 'required|numeric',
            ]
        );
        
        if ($validator->fails()) {
            return response([
                'result' => false,
                'error' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // Validate reCaptcha response
        $captcha = $params['captcha-response'] ?? null;
        if (!User::getCaptchaValidation($captcha)) {
            return $this->errors(new MessageBag([
                'recaptcha' => ApiError::ERROR_RECAPTCHA_CONFIRMATION,
            ]));
        }
        
        // Check if user lock exists
        $lock = Lock::where([
            'lock_position_id' => $params['lock_position_id'],
            'fence_position_id' => $params['fence_position_id'],
        ])->first();
        
        if (empty($lock)) {
            return response([
                'error' => ApiError::ERROR_USER_LOCK_NOT_EXISTS,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // get the user details
        $user = User::active()->where([
            'user_id' => $lock->user_id,
        ])->get()->first();
    
        // if user disabled the offers
        if (!empty($user->userDetails)
            && $user->userDetails->enable_offers !== 1
        ) {
            return response([
                'error' => ApiError::ERROR_REVIEW_OFFERS_DISABLED,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
    
        if (!empty($user)) {
            $resetToken = new ResetToken();
            $resetToken->user_id = $user->user_id;
            $token = $resetToken->generate();
            
            // set this token which helps to find who sent the request
            $userInviter->affiliate_token = $token;
            
            $emailTemplate = new UserInviteRequestEmail($user, $userInviter,
                $token);
            
            // send the email
            Mail::to($user->email)->send($emailTemplate);
        }
        
        if (!$userInviter->save()) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_GENERIC_FAIL,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        return response([
            'result' => true,
            'user' => [
                'first_name' => $user->first_name ?? null,
                'last_name' => $user->last_name ?? null,
            ]
        ]);
    }
    
    /**
     * Review a user request from inviter
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function reviewRequest(Request $request)
    {
        $params = $request->all();
        
        // User response to a received request
        $reviewed = (!empty($params['reviewed']) && $params['reviewed'] == 1);
        
        // validate fields
        $validator = Validator::make(
            $params,
            [
                'email' => 'required',
                'reviewToken' => 'required|min:16',
            ]
        );
    
        if ($validator->fails()) {
            return response([
                'result' => false,
                'errors' => $validator->errors(),
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
    
        try {
            $email = Crypt::decryptString($params['email']);
        } catch (\Exception $e) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_RESET_PASSWORD_TOKEN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // get the user details
        $user = User::active()
            ->with('couple')
            ->where([
                'email' => $email,
            ])->first();
        
        // get the inviter user details
        $userInviter = User::active()
            ->with('couple')
            ->where([
                'affiliate_token' => $params['reviewToken'],
            ])->first();
    
        // check if one of the users is not set
        if (empty($user) || empty($userInviter)) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_REVIEW_CONFIRMATION_FAIL,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }

        // check if user has a partner
        if (!empty($user->couple)) {
            return response([
                'result' => false,
                'error' => ApiError::ERROR_REVIEW_ALREADY_TAKEN,
            ])->setStatusCode(ApiError::CODE_PARAM_ERROR);
        }
        
        // if review request is accepted
        if ($reviewed === true) {
            DB::beginTransaction();
            try {
                // create a new couple
                $couple = new Couple();
                $couple->user_id = $user->user_id;
                $couple->couple_id = $userInviter->user_id;
        
                if (!$couple->save()) {
                    DB::rollBack();
                    return response([
                        'result' => false,
                        'error' => ApiError::ERROR_GENERIC_FAIL,
                    ])->setStatusCode(ApiError::CODE_ERROR);
                }
                
                $this->sendReviewAcceptedConfirmation($user, $userInviter);
        
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                return response([
                    'result' => false,
                    'error' => ApiError::ERROR_REGISTRATION_FAIL,
                ])->setStatusCode(ApiError::CODE_ERROR);
            }
        } else {
            $this->sendReviewDeclinedConfirmation($user, $userInviter);
        }
    
        $userInviter->affiliate_token = null;
        $userInviter->save();
    
        return response([
            'result' => true,
            'reviewed' => $reviewed,
            'user' => $user,
        ]);
    }
    
    /**
     * @param $user
     * @throws \Exception
     */
    private function sendAlreadyExistingEmail($user)
    {
        $resetToken = new ResetToken();
        $resetToken->user_id = $user->user_id;
        $token = $resetToken->generate();
        
        $emailTemplate = new UserAlreadyExistsEmail($user, $token);
        
        // send the email
        Mail::to($user->email)->send($emailTemplate);
    }
    
    /**
     * @param $user
     * @throws \Exception
     */
    private function sendConfirmationCode($user)
    {
        $confirmationCode = new ConfirmationCode();
        $confirmationCode->user_id = $user->user_id;
        $code = $confirmationCode->generate();
        
        $emailTemplate = new WelcomeEmail($user, $code);
        
        // send the email
        Mail::to($user->email)->send($emailTemplate);
    }
    
    /**
     * @param $user
     * @throws \Exception
     */
    private function sendResetPasswordEmail($user)
    {
        $resetToken = new ResetToken();
        $resetToken->user_id = $user->user_id;
        $token = $resetToken->generate();
        
        $emailTemplate = new ResetPasswordEmail($user, $token);
        
        // send the email
        Mail::to($user->email)->send($emailTemplate);
    }
    
    /**
     * Send a confirmation email regarding accepted review request
     *
     * @param $user
     * @param $userInviter
     * @throws \Exception
     */
    private function sendReviewAcceptedConfirmation($user, $userInviter)
    {
        $emailUserTemplate = new ReviewAcceptedConfirmation($user, $userInviter);
        $emailUserInviterTemplate = new ReviewAcceptedConfirmation($userInviter, $user);
        
        // send the email
        Mail::to($user->email)->send($emailUserTemplate);
        Mail::to($userInviter->email)->send($emailUserInviterTemplate);
    }
    
    /**
     * Send a confirmation email regarding declined review request
     *
     * @param $user
     * @param $userInviter
     * @throws \Exception
     */
    private function sendReviewDeclinedConfirmation($user, $userInviter)
    {
        $emailTemplate = new ReviewDeclinedConfirmation($user, $userInviter);
        
        // send the email
        Mail::to($userInviter->email)->send($emailTemplate);
    }
}
