<?php

namespace App\Http\Controllers;

use App\Buysic\Api\V1\ApiError;
use App\Buysic\User\User;
use App\Buysic\User\Couple;
use App\Buysic\User\Lock;
use Illuminate\Http\Request;

class LockController extends BaseApiController
{
    
    /**
     * @param $lockId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function lock($lockId)
    {
        $lock = Lock::with('user')
            ->where('lock_id', $lockId)
            ->get()
            ->first();
    
        $user = $lock->user;
        if (!empty($user)) {
            $user->story;
            $user->userDetails;
        }
        
        $couple = null;
        if (!empty($user) && !empty($user->couple)) {
            $couple = User::find($user->couple->couple_id);
            $couple->story;
            $couple->userDetails;
        }
        
        if ($lock) {
            return response([
                'result' => true,
                'lock' => $lock,
                'couple' => $couple,
            ]);
        }
        
        return response([
            'message' => ApiError::ERROR_ELEMENT_NOT_FOUND,
        ], ApiError::CODE_NOT_FOUND);
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function locks(Request $request)
    {
        $pageNumber = $request->input('page');
        $fencesPerPage = $request->input('fences');
        
        // Show more results in the first page
        $totalResult = $fencesPerPage * 10;
        
        $locks = Lock::with('user')
            ->orderBy('fence_position_id')
            ->orderBy('lock_position_id')
            ->paginate($totalResult, ['*'], 'page', $pageNumber);
        
        return response([
            'total_results' => $locks->count(),
            'locks' => $locks,
        ]);
    }
    
    /**
     * @param $userId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function userLock($userId)
    {
        $lock = Lock::where('user_id', $userId)
            ->get();
        if ($lock) {
            return response($lock);
        }
        
        return response([
            'message' => ApiError::ERROR_ELEMENT_NOT_FOUND,
        ], ApiError::CODE_NOT_FOUND);
    }
    
    /**
     * @param $lockId
     * @param $userId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function search($lockId, $userId)
    {
        $lock = Lock::where('lock_id', $lockId)
            ->where('user_id', $userId)
            ->get()
            ->first();
        
        if ($lock) {
            return response($lock);
        }
        
        return response([
            'message' => ApiError::ERROR_ELEMENT_NOT_FOUND,
        ], ApiError::CODE_NOT_FOUND);
    }
}
