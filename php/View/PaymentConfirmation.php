<?php

namespace App\Buysic\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaymentConfirmation extends Mailable
{
    use Queueable,
        SerializesModels,
        EmailTrait;

    public $subject;
    public $user;
    public $siteUrl;

    /**
     * Create a new message instance.
     *
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
        
        $this->siteUrl = config('app.web_url');
        
        $this->subject = config('app.mail.from_name') . ' - Your payment has been '
            . 'successfully received';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.paymentConfirmation');
    }
}
