<?php

namespace App\Buysic\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class WelcomeEmail extends Mailable
{
    use Queueable,
        SerializesModels,
        EmailTrait;

    public $subject;
    public $user;
    public $siteUrl;
    public $loginUrl;
    public $activateUrl;
    public $code;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $code
     */
    public function __construct($user, $code)
    {
        $this->user = $user;
        $this->code = $code;

        $this->siteUrl = config('app.web_url');
        $this->loginUrl = $this->siteUrl . '/login/';
        $this->activateUrl
            = $this->siteUrl
            . '/?email='
            . Crypt::encryptString($user->email)
            .'&code='
            . hash('sha256', $this->code);
        
        $this->subject = config('app.mail.from_name') . ' - Thanks for registering';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome');
    }
}
