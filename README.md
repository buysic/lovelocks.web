### Million Love Locks
**Hi there!**

I want to share some bits of code from my personal project I was recently working on.

**Brief project description**

Alex has designed this idea for people who want to share their love and hang their love locks on the digital bridge and own a piece of love story forever on the internet! This is a bridge of love with just one million available hearts!
Hang your love lock and own a piece of love story forever on the internet!


**Demo URL**

`https://www.millionlovelocks.com/`

If anything goes wrong or you have any questions please don't hesitate to get in touch.

Thank you!