import Modal from "./Modal";
import Api from "../util/Api";
import UserStoryModal from "../modals/UserStoryModal";
import SelectLockModal from "../modals/SelectLockModal";
import User from "./User";

class Lock
{

    /**
     * Draw a single lock
     *
     * @param array
     * @param pageNum
     * @param fencesPerPage
     */
    static draw(array = [], pageNum = 1, fencesPerPage = 10)
    {
        let locks = '<img src="public/images/fence_inner.png">';
        let itemsProcessed = 0;
        array.forEach((item, index) => {
            setTimeout(() => {
                let fenceId = index + 1;
                let image = '<img class="heart" src="public/images/heart.gif" />';
                if (item.lock_type_id) {
                    image = '<img ' +
                        'src="public/images/lock' + item.lock_type_id + '.png" ' +
                        'data="' + item.lock_id + '" ' +
                        'data-toggle="modal" ' +
                        'data-target="#openModal" />';
                }

                locks += '' +
                    '<span class="lock' + item.lock_position_id + '">' +
                        image +
                    '</span>';

                // Append 10 locks at once for every fence
                if (fenceId % 10 === 0) {
                    let element = document.getElementById(item.fence_position_id);
                    if (element !== null) {
                        element.innerHTML = locks;
                        locks = '<img src="public/images/fence_inner.png">';
                    }
                }

                // Once finished - highlight the active lock
                itemsProcessed++;
                if (itemsProcessed === array.length) {
                    this.highlightActiveLock();
                }
            }, index * 10);
        });
    }

    /**
     * Highlight the selected/active lock
     *
     * @param fenceId
     * @param lockId
     */
    static highlightActiveLock(fenceId, lockId)
    {
        let fenceEle = document.getElementById('fenceId');
        let lockEle = document.getElementById('lockId');
        let fencePositionId = fenceId
            ? fenceId
            : (fenceEle != null) ? fenceEle.value
            : null;
        let lockPositionId = lockId
            ? lockId
            : (lockEle != null) ? lockEle.value
            : null;

        if (fencePositionId && lockPositionId) {
            $('#' + fencePositionId + ' ' + '.lock' + lockPositionId).addClass('active');
        }
    }

    /**
     * API call to retrieve data from a specific lock
     *
     * @param data
     * @param url
     */
    static view(data, url)
    {
        Api.call(
            data,
            url,
            'GET',
            false,
            (result) => {
                let template = UserStoryModal.show(result);
                Modal.load(template);
            }
        );
    }

    /**
     * Select the lock before purchase
     *
     * @param data
     */
    static select(data)
    {
        User.profile((result) => {
            let template = null;
            let user = result.user;

            if (user) {
                template = SelectLockModal.show(data);
                Modal.load(template);
            } else {
                $('#modalSignIn').modal('show');
            }
        });
    }
}

export default Lock;
