import Api from "../util/Api";
import Lock from "./Lock";

let fenceCount = 50,
    currentIndex = null,
    swipeDirection = null;

/* Deprecated class */
class Fence
{

    /**
     * Instantiate carousel
     */
    static carousel()
    {
        return $('.carousel').flickity();
    }

    /**
     * Instantiate flickity
     */
    static flkty()
    {
        return this.carousel().data('flickity');
    }

    /**
     * Draw each fence element
     */
    static draw(totalElements = 10, direction = 'left', minEle, maxEle)
    {
        let fenceElements = '';
        if (direction === 'left') {
            fenceCount = maxEle;
        } else if (direction === 'right') {
            fenceCount = minEle;
        }

        for (let i=0; i < totalElements; i++) {
            if (direction === 'left') {
                fenceCount ++;
            } else if (direction === 'right') {
                fenceCount --;
                if (fenceCount < 1) {
                    break;
                }
            }
            fenceElements +=
                '<div class="carousel-cell lock-container" id="' + fenceCount + '">' +
                    '<img src="public/images/fence_inner.png" />' +
                    '<span class="lock1"><img class="heart" src="public/images/heart.gif" /></span>' +
                    '<span class="lock2"><img class="heart" src="public/images/heart.gif" /></span>' +
                    '<span class="lock3"><img class="heart" src="public/images/heart.gif" /></span>' +
                    '<span class="lock4"><img class="heart" src="public/images/heart.gif" /></span>' +
                    '<span class="lock5"><img class="heart" src="public/images/heart.gif" /></span>' +
                    '<span class="lock6"><img class="heart" src="public/images/heart.gif" /></span>' +
                    '<span class="lock7"><img class="heart" src="public/images/heart.gif" /></span>' +
                    '<span class="lock8"><img class="heart" src="public/images/heart.gif" /></span>' +
                    '<span class="lock9"><img class="heart" src="public/images/heart.gif" /></span>' +
                    '<span class="lock10"><img class="heart" src="public/images/heart.gif" /></span>' +
                '</div>';
        }

        return fenceElements;
    }

    /**
     * This method fires on flickity drag event and after load more fence elements
     * based on the current position status
     *
     * @param selectedIndex
     */
    static drag(selectedIndex)
    {
        if (selectedIndex !== currentIndex) {
            // We moved to a new cell
            console.log('Swipe successful to index ' + this.flkty().selectedIndex);
            console.log(this.getCurrentPage() + '/' + this.getTotalPages() + ' (' + this.getStatus() + '%)');

            let pageNum = 1;
            let ele = $('.carousel-cell');
            let totalElements = ele.length;
            let ids = ele.map(function() {
                return $(this).attr('id');
            }).get();

            let minEle = Math.min.apply(Math, ids);
            let maxEle = Math.max.apply(Math, ids);

            if (this.getStatus() > 70 && swipeDirection === 'left') {
                pageNum++;
                axios.get(Api.host() + '/locks/?page=' + pageNum + '&' + Api.key())
                    .then(response => {
                        this.locks = response.data.locks.data;
                        Lock.draw(this.locks);
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });

                let fenceElements = $(Fence.draw(10, swipeDirection, minEle, maxEle));
                this.carousel().flickity('append', fenceElements);

                if (totalElements >= 50) {
                   // $('div.carousel-cell:lt(10)').remove();
                }

                console.log('LOADED > 70');
            } else if (this.getStatus() < 20 && swipeDirection === 'right') {
                pageNum--;
                axios.get(Api.host() + '/locks/?page=' + pageNum + '&' + Api.key())
                    .then(response => {
                        this.locks = response.data.locks.data;
                        Lock.draw(this.locks);
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });

                let fenceElements = $(Fence.draw(10, swipeDirection, minEle, maxEle));

                if (totalElements >= 50) {
                    $('div.carousel-cell').slice(-10).remove();
                }
            }

            // TODO: Remove this for production
            if (swipeDirection === 'left') {
                console.log('Swiped left');
            } else {
                console.log('Swiped right');
            }
        } else {
            // We swiped but not enough to move to the next cell
            //console.log('swipe failed');
        }
    }

    /**
     * Set the swipe direction
     *
     * @param selectedIndex
     * @param moveVector
     */
    static setSwipeDirection(selectedIndex, moveVector)
    {
        currentIndex = selectedIndex;
        swipeDirection = this.getSwipeDirection(moveVector);
    }

    /**
     * Determine which direction is swiped
     *
     * @param moveVector
     */
    static getSwipeDirection(moveVector)
    {
        let swipeDirection = null;

        if (moveVector.x > 0) {
            swipeDirection = 'right';
        } else {
            swipeDirection = 'left';
        }

        return swipeDirection;
    }

    /**
     * Get current page of flickity element
     */
    static getCurrentPage()
    {
        return this.flkty().selectedIndex + 1;
    }

    /**
     * Get total pages of flickity elements
     */
    static getTotalPages()
    {
        return this.flkty().slides.length;
    }

    /**
     * Get status of flickity elements in percentage
     */
    static getStatus()
    {
        return Math.floor((this.getCurrentPage() / this.getTotalPages()) * 100);
    }
}

export default Fence;
