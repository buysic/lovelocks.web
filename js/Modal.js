class Modal
{

    /**
     * Load data into modal
     *
     * @param data
     * @param title
     */
    static load(data = null, title = null)
    {
        let $msgModal = $('#openModal').modal({
                backdrop : true,
                show : true,
                keyboard : true
        }),
        showMsg = (text, header, callback) => {
            $msgModal
                .find('.modal-header > h5').text(header).end()
                .find('.modal-body').html(text).end()
                .find('.callback-btn').off('click.callback')
                .on('click.callback', callback).end()
                .modal('show');
        };

        if (data) {
            showMsg(data, title);
        }
    }

    /**
     * Show loading spinner in the modal
     */
    static showLoader()
    {
        // hide the initial loader if any exist
        this.hideLoader();
        $('.modal-content').append('' +
            '<div class="modal-loader">' +
                '<span class="fa fa-circle-o-notch fa-spin fa-2x"></span>' +
            '</div>'
        );
    }

    /**
     * Hide loading spinner in the modal
     */
    static hideLoader()
    {
        $('.modal-loader').fadeOut('normal', function() {
            $(this).remove();
        });
    }
}

export default Modal;
