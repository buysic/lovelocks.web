const tag = document.createElement('script');
tag.src = '//youtube.com/player_api';
const firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
const videoId = '6_K6lxxi4t0'; //jEnd8JIMii4

let bg,
    playerDefaults = {
        autoplay: 1,
        autohide: 1,
        loop: 1,
        modestbranding: 0,
        wmode: 'transparent',
        rel: 0,
        fs: 0,
        showinfo: 0,
        controls: 0,
        disablekb: 1,
        enablejsapi: 0,
        iv_load_policy: 3,
        playlist: videoId,
        origin: window.location.href,
    };

const vid = [
        {
            'videoId': videoId,
            'startSeconds': 3,
            'endSeconds': 1400,
            'suggestedQuality': 'hd720',
        },
    ],
randomVid = Math.floor(Math.random() * vid.length),
currVid = randomVid;

function onYouTubePlayerAPIReady() {
    bg = new YT.Player('bg', {
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }, playerVars: playerDefaults
    });
}

function onPlayerReady(event) {
    bg.loadVideoById(vid[currVid]);
    event.target.playVideo();
}

function onPlayerStateChange(event) {
    $('#bg').addClass('active');

    if (event.data === YT.PlayerState.PLAYING) {
        hideLoader();
    }
    if (event.data === YT.PlayerState.ENDED) {
        bg.seekTo(0);
        bg.playVideo();
    }
}

function vidRescale() {
    let w = $(window).width() + 200,
        h = $(window).height() + 200;
    if (w / h > 16 / 9) {
        bg.setSize(w, w / 16 * 9);
        $('.bg .screen').css({'left': '0px'});
    } else {
        bg.setSize(h / 9 * 16, h);
        $('.bg .screen').css({'left': -($('.bg .screen').outerWidth() - w) / 2});
    }
}

function hideLoader() {
    $('body').removeClass('loading');
}

$(window).on('load resize', function () {
    vidRescale();
});
