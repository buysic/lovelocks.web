import Api from "../util/Api";
import Notification from "../util/Notification";
import Cookie from "../util/Cookie";
import Form from "../util/Form";

class User
{

    /**
     * Get user profile details
     *
     */
    static profile(callEndpoint)
    {
        Api.call(
            null,
            '/profile',
            'GET',
            false,
            (response) => {
                callEndpoint(response);
            },
            (response) => {
                callEndpoint(response);
            }
        );
    }

    /**
     * Register a new user
     *
     * @param data
     * @param url
     */
    static register(data, url)
    {
        Api.call(
            data,
            url,
            'POST',
            false,
            () => {
                $('.form-signup')[0].reset();
                $('#modalSignUp').modal('hide');
                Notification.show('You have been successfully registered. ' +
                    'Please check your email for further instructions.');
            },
            (response) => {
                Form.validateFields('form-signup', response);
            }
        );
    }

    /**
     * Login an existing user
     *
     * @param data
     * @param url
     */
    static login(data, url)
    {
        Api.call(
            data,
            url,
            'POST',
            false,
            (response) => {
                $('.form-signin')[0].reset();
                $('#modalSignIn').modal('hide');

                if (response && response.jwt) {
                    Cookie.set('token', response.jwt);
                    Notification.show(`Welcome back, ${response.user.first_name}!`);
                    location.reload(false);
                }
            },
            (response) => {
                Form.validateFields('form-signin', response);
            }
        );
    }

    /**
     * Send a password reminder
     *
     * @param data
     * @param url
     */
    static forgot(data, url)
    {
        Api.call(
            data,
            url,
            'POST',
            false,
            () => {
                $('.form-forgot')[0].reset();
                $('#modalForgot').modal('hide');
                Notification.show('Success! Please check your email for further information.');
            },
            (response) => {
                Form.validateFields('form-forgot', response);
            }
        );
    }

    /**
     * Reset the user's password
     *
     * @param data
     * @param url
     */
    static resetPassword(data, url)
    {
        Api.call(
            data,
            url,
            'POST',
            false,
            () => {
                $('#modalReset').modal('hide');
                Notification.show('Your password has been successfully changed. Please log in.');
            },
            (response) => {
                Form.validateFields('form-reset', response);
            }
        );
    }

    /**
     * Confirm user email and activate account
     *
     * @param data
     * @param url
     */
    static confirmEmail(data, url)
    {
        Api.call(
            data,
            url,
            'POST',
            false,
            (response) => {
                if (response.jwt) {
                    Cookie.set('token', response.jwt);
                    Notification.show('Your account has been successfully ' +
                        'activated!');
                }
            },
            (response) => {
                if (response && response.error) {
                    Notification.show(response.error, 'error');
                }
            }
        );
    }

    /**
     * Update user's profile
     *
     * @param data
     * @param url
     */
    static updateProfile(data, url)
    {
        Api.call(
            data,
            url,
            'POST',
            false,
            (response) => {
                if (response && response.result === true) {
                    $('#modalProfile').modal('hide');
                    Notification.show('Your account has been successfully ' +
                        'updated!');
                }
            },
            (response) => {
                Form.validateFields('form-profile', response);
            }
        );
    }

    /**
     * Send a request to user
     *
     * @param data
     * @param url
     */
    static sendRequest(data, url)
    {
        Api.call(
            data,
            url,
            'POST',
            false,
            (response) => {
                let userName = (response && response.user.first_name) ?
                    '<b>' + response.user.first_name + '</b>' :
                    'This user';
                if (response && response.result === true) {
                    Notification.show('Your request has been successfully sent ' +
                        'to ' + userName + '! The person will receive your ' +
                        'request and review it shortly.');
                }
            },
            (response) => {
                Form.validateFields('form-request', response);
            }
        );
    }

    /**
     * Review a user request from inviter
     *
     * @param data
     * @param url
     */
    static reviewRequest(data, url)
    {
        Api.call(
            data,
            url,
            'POST',
            false,
            (response) => {
                if (response && response.result === true) {
                    $('#modalReview').modal('hide');
                    if (response.reviewed === true) {
                        Notification.show('Congratulations! The request has ' +
                            'been successfully approved and your account is ' +
                            'now linked with ' +
                            '<b>' + response.user.first_name + '</b>!'
                        );
                    } else {
                        Notification.show('You have successfully declined ' +
                            'this request.');
                    }
                }
            },
            (response) => {
                $('#modalReview').modal('hide');
                if (response && response.error) {
                    Notification.show(response.error, 'error');
                } else {
                    Notification.show('Whoops! Something went wrong. Please try ' +
                        'again later.', 'error');
                }
            }
        );
    }
}

export default User;
