import Notification from "./Notification";
import Captcha from "./Captcha";

class Form
{

    /**
     * Submit form to API Endpoint
     *
     * @param formClass
     * @param url
     * @param callEndpoint
     */
    static submit(formClass = 'form', url = '/', callEndpoint = null)
    {
        let form = $('.' + formClass);
        form.submit(function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            let data = $(this).serializeArray();
            let captchaResponse = document.getElementById("captcha-response").value;

            data.push({name: 'captcha-response', value: captchaResponse});

            if (callEndpoint) {
                callEndpoint(data, url);
            }
        });
    }

    /**
     * Form fields validation
     *
     * @param formClass
     * @param response
     * @param handleCaptcha
     */
    static validateFields(formClass = 'form', response = {}, handleCaptcha = true)
    {
        let errors = response.errors || {};
        $.each(errors, (field, error) => {
            let input = $('.' + formClass + ' [name="'+ field +'"]');
            if (error) {
                input.addClass('is-invalid');
                input.siblings('.invalid-feedback').html(error);
            } else {
                input.removeClass('is-invalid');
                input.siblings('.invalid-feedback').html('');
            }
        });

        // Handle singleton errors
        if (response.error) {
            Notification.show(response.error, 'error');
        } else if (handleCaptcha && errors.recaptcha) {
            Captcha.execute().then(function() {
                Notification.show(errors.recaptcha, 'error');
            }).catch(function () {
                Notification.show(errors.recaptcha, 'error');
            });
        }
    }
}

export default Form;
