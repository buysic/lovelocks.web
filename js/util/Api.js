import Cookie from "./Cookie";
import Modal from "../partials/Modal";

class Api
{

    /**
     * Default API host address
     */
    static host()
    {
        return process.env.API_URL;
    }

    /**
     * Default API account key and signature
     */
    static key()
    {
        return 'sig=' + process.env.API_SIG + '&' + 'apiKey=' + process.env.API_KEY;
    }

    /**
     * Abstract API call method
     *
     * @param params
     * @param type
     * @param url
     * @param authRequired
     * @param successCallbackFn
     * @param failCallbackFn
     */
    static call(params, url, type = 'GET', authRequired = false, successCallbackFn = null, failCallbackFn = null)
    {
        Modal.showLoader();
        $.ajax({
            type     : type,
            url      : url
                ? Api.host() + url + '?' + Api.key()
                : process.env.WEB_URL + '?action=callApi',
            data     : params,
            dataType : 'json',
            encode   : true,
            cache    : false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Bearer', Cookie.get('token'));
            },
        }).done(function(data) {
            if (data) {
                if (successCallbackFn != null) {
                    successCallbackFn(data);
                }
            } else {
                if (failCallbackFn != null) {
                    failCallbackFn(data);
                }
            }
            Modal.hideLoader();
        }).fail(function(error) {
            if (error && failCallbackFn) {
                failCallbackFn(error.responseJSON);
            }
            Modal.hideLoader();
        });
    }
}

export default Api;
