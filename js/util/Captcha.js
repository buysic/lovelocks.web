import Notification from "../util/Notification";

class Captcha
{

    /**
     * Google site key
     */
    static key()
    {
        // TODO: change this to real Google site key
        return 'xxxxxxxxxxxxxxxxxxxxxxx';
    }

    /**
     * Execute Google captcha
     */
    static async execute()
    {
        await grecaptcha.execute(Captcha.key(), {action: 'homepage'})
            .then(function (token) {
                $('#captcha-response').val(token);
            })
            .catch(function (e) {
                Notification.show(e, 'error');
            });
    }

}

export default Captcha;
