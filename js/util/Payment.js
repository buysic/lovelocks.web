import Modal from "../partials/Modal";
import Notification from "./Notification";
import Api from "./Api";
import Form from "./Form";

class Payment
{

    /**
     * Set an environment
     *
     * @options test
     * @options live
     */
    static environment() {
        return 'live';
    }

    /**
     * Stripe API key
     *
     * @param env - an environment
     */
    static apiKey(env = Payment.environment()) {
        if (env === 'live') {
            return 'pk_live_a9sEyN1mBPJz07Mk7NL1mw6U';
        }

        return 'pk_test_IPL8lS5A3FOETrivedOwUPOO';
    }

    /**
     * Instantiate card object
     */
    static initCard() {
        new Card({
            form: document.querySelector('.form-checkout'),
            container: '.card',
            formSelectors: {
                numberInput: 'input[name=number]',
                expiryInput: 'input[name=expiry]',
                cvcInput: 'input[name=cvc]',
                nameInput: 'input[name=full_name]'
            },
            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'Full Name',
                expiry: '••/••',
                cvc: '•••'
            }
        });
    }

    /**
     * Show Stripe checkout dialog
     *
     * @param ele - checkout button
     */
    static v1(ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            Modal.showLoader();

            let handler = StripeCheckout.configure({
                key: Payment.apiKey(),
                image: 'https://brainring.co.uk/img/logo.png',
                locale: 'auto',
                token: function (token) {
                    console.log(token);
                    // API CALL HERE
                    //$('input[name="token"]').val(token.id);
                    //$('input[name="email"]').val(token.email);
                    //$("#form-checkout").submit();
                }
            });

            handler.open({
                name: 'Store My Love',
                description: 'All about sharing the love.',
                shippingAddress: false,
                billingAddress: true,
                zipCode: true,
                currency: 'usd',
                allowRememberMe: false,
                amount: Math.floor(2.00 * 100),
                opened: () => {
                    customModal.modal('hide');
                    Modal.hideLoader();
                },
                closed: () => {
                    Modal.hideLoader();
                },
            });

            // Close Checkout on page navigation:
            window.addEventListener('popstate', function () {
                handler.close();
            });
        });
    }

    /**
     * Stripe payment v2
     */
    static v2() {
        if (window.Stripe) {
            Stripe.setPublishableKey(Payment.apiKey());
        } else {
            document.querySelector('#stripe-js').addEventListener('load', () => {
                Stripe.setPublishableKey(Payment.apiKey());
            });
        }

        this.initCard();

        document.querySelector('.form-checkout').addEventListener('submit', function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            Modal.showLoader();

            // Split month and year from expiry field
            let expirationDate = $('input[name=expiry]').val();
            let MMYY = expirationDate.match(/([0-9]{2}) \/ ([0-9]{2})/);
            let MMYYYY = expirationDate.match(/([0-9]{2}) \/ ([0-9]{4})/);
            let expiryMonth = MMYYYY ? MMYYYY[1] : MMYY ? MMYY[1] : '';
            let expiryYear = MMYYYY ? MMYYYY[2] : MMYY ? MMYY[2] : '';

            Stripe.card.createToken({
                name: $('input[name=full_name]').val(),
                number: $('input[name=number]').val(),
                exp_month: expiryMonth,
                exp_year: expiryYear,
                cvc: $('input[name=cvv]').val(),
            }, Payment.stripeResponseHandler);
        });
    }

    /**
     * Handle the responses from Stripe API
     */
    static stripeResponseHandler(status, response) {
        if (response.error) {
            Modal.hideLoader();
            Notification.show(response.error.message, 'error');
        } else {
            const data = {
                token: response.id,
                cvc: document.getElementById("cardCVC").value.replace(/\d/g, "*"),
                full_name: document.getElementById("fullName").value,
                fence_position_id: document.getElementById("fencePositionId").value,
                lock_position_id: document.getElementById("lockPositionId").value,
                lock_type_id: document.getElementById("lockTypeId").value,
            };
            Api.call(
                data,
                '/payment/stripe',
                'POST',
                false,
                (response) => {
                    $('.form-checkout')[0].reset();
                    $('#modalCheckout').modal('hide');

                    if (response && response.result === true) {
                        Notification.show('Your payment has been successfully ' +
                            'completed.');
                    }
                },
                (response) => {
                    Form.validateFields('form-checkout', response);
                }
            );
        }
    };

    /**
     * Stripe payment v3
     */
    static v3() {
        let stripe = {};

        if (window.Stripe) {
            stripe = Stripe(Payment.apiKey());
        } else {
            document.querySelector('#stripe-js').addEventListener('load', () => {
                stripe = Stripe(Payment.apiKey());
            });
        }

        const elements = stripe.elements({
            fonts: [
                {
                    color: '#495057',
                    fontWeight: 500,
                    fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                },
            ],
            locale: 'auto'
        });

        const elementStyles = {
            base: {
                iconColor: '#66bb6a',
                color: '#495057',
                fontWeight: 500,
                lineHeight: '32px',
                fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                fontSize: '16px',
                fontSmoothing: 'antialiased',
                '::placeholder': {
                    color: '#495057',
                },
                ':-webkit-autofill': {
                    color: '#495057',
                },
            },
            invalid: {
                iconColor: '#ea2362',
                color: '#ea2362',

                '::placeholder': {
                    color: '#ea2362',
                },
            },
        };

        const elementClasses = {
            focus: 'focused',
            empty: 'empty',
            invalid: 'invalid',
        };

        const cardNumber = elements.create('cardNumber', {
            style: elementStyles,
            classes: elementClasses,
        });
        cardNumber.mount('#card-number');

        const cardExpiry = elements.create('cardExpiry', {
            style: elementStyles,
            classes: elementClasses,
        });
        cardExpiry.mount('#card-expiry');

        const cardCvc = elements.create('cardCvc', {
            style: elementStyles,
            classes: elementClasses,
        });
        cardCvc.mount('#card-cvc');

        // Validation
        if (Payment.validate(cardNumber)) {
            if (Payment.validate(cardExpiry)) {
                Payment.validate(cardCvc);
            }
        }

        document.querySelector('.form-checkout').addEventListener('submit', function(e) {
            e.preventDefault();
            let form = document.querySelector('.form-checkout');
            let extraDetails = {
                full_name: form.querySelector('input[name="full_name"]').value,
            };
            stripe.createToken(cardNumber, extraDetails).then(Payment.setOutcome);
        });
    }

    /**
     * Set a success/fail response from Stripe
     *
     * @param response
     */
    static setOutcome(response) {
        if (response.token) {
            Notification.show(response.token.id);
        } else if (response.error) {
            Notification.show(response.error.message, 'error');
            return false;
        }

        return true;
    }

    /**
     * Validate payment fields
     *
     * @param ele
     */
    static validate(ele) {
        if (ele && ele instanceof Object) {
            ele.on('change', function(event) {
                Payment.setOutcome(event);
            });
        }
    }
}

export default Payment;
