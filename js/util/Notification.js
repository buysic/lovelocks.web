class Notification
{

    /**
     * Show a notification message
     *
     * @link https://github.com/CodeSeven/toastr/
     *
     * @param message
     * @param type
     * @param options
     */
    static show(message, type = 'success', options = {})
    {
        let params = options || {};
        params.closeButton = true;
        params.positionClass = 'toast-bottom-full-width';
        params.progressBar = true;
        params.timeOut = 7000;
        params.extendedTimeOut = 5000;

        toastr.options = params;
        toastr[type](message);
    }

}

export default Notification;
